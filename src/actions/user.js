export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST'
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS'
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE'

export function requestUserLogin(username, password) {
  return {
    type: LOGIN_USER_REQUEST,
    username,
    password
  }
}