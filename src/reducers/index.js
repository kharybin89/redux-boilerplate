import { combineReducers } from 'redux'
import user from '../reducers/user'
import office from '../reducers/office'
import { routerReducer as routing } from 'react-router-redux'

export default combineReducers({
  user,
  office,
  routing
})