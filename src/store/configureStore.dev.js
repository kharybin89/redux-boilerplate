import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import rootReducer from '../reducers'
import DevTools from '../containers/DevTools'

const configureStore = preloadedState => {
  const store = createStore(
    rootReducer,
    preloadedState,
    compose(
      applyMiddleware(thunk, createLogger()),
      DevTools.instrument()
    )
  )

  // Эту переменную выставляет webpack Hot Module Replacement плагин
  if (module.hot) {
    // Чтобы горячая перезагрузка (Hot Module Replacement) заработала
    // для редьюсеров подменяем старый редьюсер в сторе на новый
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').default
      // Записываем в store новый редьюсер
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}

export default configureStore
