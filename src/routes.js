import React from 'react'
import { Route } from 'react-router'
import MainPage from './containers/MainPage'
import LoginPage from './containers/LoginPage'

export default <Route>
  <Route path="/" component={MainPage} />
  <Route path="/login" component={LoginPage} />
</Route>
