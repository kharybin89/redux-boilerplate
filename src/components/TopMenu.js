import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'

class TopMenu extends Component {
  render() {
    const user = this.props.user

    return (
      <div>
        {
          user.isLoggedIn
            ? <Link to="/login">Login</Link>
            : <Link to="/profile">Profile</Link>
        }
      </div>
    )
  }
}

TopMenu.propTypes = {
  user: PropTypes.object.isRequired
}

export default TopMenu
