import React, { Component, PropTypes } from 'react'

class TopMenu extends Component {
  render() {
    const user = this.props.user

    return (
      <div>
        {user.isLoggedIn ? 'ok' : 'ne ok'}
      </div>
    )
  }
}

TopMenu.propTypes = {
  user: PropTypes.object.isRequired
}

export default TopMenu
