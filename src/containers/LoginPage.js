import React, { Component } from 'react'
import { connect } from 'react-redux'
import { requestUserLogin } from '../actions/user'

class LoginPage extends Component {
  handleLogin() {
    return requestUserLogin(
      this.refs.username.value.trim(),
      this.refs.password.value.trim()
    )
  }

  render() {
    const { dispatch } = this.props
    return (
      <div>
        <h3>Login page</h3>
        <input ref="username" type="text" placeholder="Email" />
        <input ref="password" type="password" placeholder="Пароль" />
        <button onClick={() => dispatch(this.handleLogin()) }>
            Login
        </button>
      </div>
    )
  }
}

export default connect()(LoginPage)