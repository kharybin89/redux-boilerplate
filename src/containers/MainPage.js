import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import TopMenu from '../components/TopMenu'

class MainPage extends Component {
  render() {
    return (
      <div>
        <TopMenu user={this.props.user}/>
      </div>
    )
  }
}

MainPage.propTypes = {
  user: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(MainPage)