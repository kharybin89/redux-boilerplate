import React from 'react'
import '../frontend/css/main.css'
import { render } from 'react-dom'
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import Root from './containers/Root'
import configureStore from './store/configureStore'

const store = configureStore()
const history = syncHistoryWithStore(browserHistory, store)
module.hot.accept()
render(
  <Root store={store} history={history} />,
  document.getElementById('app')
)