var path = require('path')
var webpack = require('webpack')

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  // Точка входа в webpack, обрабатывает эти файлы и результат собирает в бандл
  entry: [
    // Это коннектит клиент с сервером,
    // чтобы получать уведомления, когда банды пересобирается и обновить бандл на клиенте
    // В отличии от лив релод, страница не перезагрузится, а сделает обновление без перезагрузки
    'webpack-hot-middleware/client',
    // Точка входа в реакт приложение
    './src/index',
    // Добавление поддержки ES2015 функций в браузер
    'babel-polyfill'
  ],
  // Тут описывается как записать скомпилированные файлы на диск
  output: {
    path: path.join(__dirname, 'static'),
    filename: 'bundle.js',
    publicPath: '/static/' // Публичный путь используется loader'ами для путей в тегах link и script
  },
  module: {
    preLoaders: [ //добавили ESlint в preloaders
      {
        test: /\.js$/,
        loaders: ['eslint'],
        include: [
          path.resolve(__dirname, 'src')
        ]
      }
    ],
    // Loaders - это функции, которые исполняет Nodejs,
    // на вход которым дается контент файла и возвращается новый контент
    // Loader'ы работают как декораторы, которые обрабатывают ресурс и передают следующем loader'у
    // Loader'ы обрабытваются справа налево(!)
    loaders: [ //добавили babel-loader
      {
        // react-hot нужен для HMR компонентов React'а
        // если не добавить, то в браузере пояаится сообщение, что HMR не знает как обновить компонент
        loaders: ['babel-loader'],
        // Все .js файлы в директории src будут обрабатываться эти loader'ом
        include: [
          path.resolve(__dirname, 'src'),
        ],
        test: /\.js$/,
        plugins: ['transform-runtime'],
      },
      {
        test:   /\.css$/,
        loader: 'style!css'
      },
    ]
  },
  // Плагины - вещь более мощная, чем loader'ы. Загрузчики работают с конкретными файлом, плагины обычно работают со всем бандлом.
  // Например, если нам нужно минифицировать уже скомплированный bundle.js.
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': '"development"'
      }
    })
  ]
}