var app = new (require('express'))()
var port = 3001

var items = function generateItems() {
  var result = []
  var titles = ['Foo', 'Bar', 'Baz']

  for (var i = 1; i <= 100; i++) {
    var item = {
      id: i,
      title: titles[Math.floor(Math.random() * titles.length)]
    }

    result.push(item)
  }

  return result
}()

app.use('/items', function (req, res) {
  res.send(items)
})

app.listen(port, function(error) {
  if (error) {
    console.error(error)
  } else {
    console.info('==> Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port)
  }
})