var path = require('path')
var webpack = require('webpack')

module.exports = {
  entry: [
    './src/index',
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/' // Публичный путь используется loader'ами для путей в тегах link и script
  },
  module: {
    preLoaders: [
    {
      test: /\.js$/,
      loaders: ['eslint'],
      include: [
        path.resolve(__dirname, 'src'),
      ],
    }
    ],
    loaders: [ //добавили babel-loader
      {
        loaders: ['babel-loader'],
        include: [
          path.resolve(__dirname, 'src'),
        ],
        test: /\.js$/,
      },
      {
        test:   /\.css$/,
        loader: 'style!css'
      },
    ]
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': '"production"'
      }
    })
  ]
}